namespace PuzzleSolver;

public static class Board
{
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   + JAN + FEB + MAR + APR + MAY + JUN + XXX +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   + JUL + AUG + SEP + OCT + NOV + DEC + XXX +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   +   1 +   2 +   3 +   4 +   5 +   6 +   7 +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   +   8 +   9 +  10 +  11 +  12 +  13 +  14 +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   +  15 +  16 +  17 +  18 +  19 +  20 +  21 +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   +  22 +  23 +  24 +  25 +  26 +  27 +  28 +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //   +  29 +  30 +  31 + XXX + XXX + XXX + XXX +
    //   +-----+-----+-----+-----+-----+-----+-----+
    //
    //
    //   We represent the board as an array of constants.
    //   - we use the integers 1 .. 31 for the days
    //   - we use the numbers 32 .. 43 for the months
    //   - we use 0 for the boxes that are not in use
    
    public static readonly int[,] Values =  
    {
        { 32, 33, 34, 35, 36, 37,  0},
        { 38, 39, 40, 41, 42, 43,  0},
        {  1,  2,  3,  4,  5,  6,  7},
        {  8,  9, 10, 11, 12, 13, 14},
        { 15, 16, 17, 18, 19, 20, 21},
        { 22, 23, 24, 25, 26, 27, 28},
        { 29, 30, 31,  0,  0,  0,  0} 
    };

    //   We invert the board to a one dimensional array of coordinates. 
    //   This makes it more efficient for iterating through all possible positions.
    //   Also, using this notation, we can completely ignore the unused boxes.
    //   We define an array of 43 elements: 1..31 for the days, and 32..43 for the months
    //   The elements of the array are the coordinates of the days and months as in the above board

    internal static readonly RowCol[] Positions =
    {
        /* 1*/ new RowCol(2,0), /* 2*/ new RowCol(2,1), /* 3*/ new RowCol(2,2), /* 4*/ new RowCol(2,3), /* 5*/ new RowCol(2,4), /* 6*/ new RowCol(2,5),
        /* 7*/ new RowCol(2,6), /* 8*/ new RowCol(3,0), /* 9*/ new RowCol(3,1), /*10*/ new RowCol(3,2), /*11*/ new RowCol(3,3), /*12*/ new RowCol(3,4),
        /*13*/ new RowCol(3,5), /*14*/ new RowCol(3,6), /*15*/ new RowCol(4,0), /*16*/ new RowCol(4,1), /*17*/ new RowCol(4,2), /*18*/ new RowCol(4,3),
        /*19*/ new RowCol(4,4), /*20*/ new RowCol(4,5), /*21*/ new RowCol(4,6), /*22*/ new RowCol(5,0), /*23*/ new RowCol(5,1), /*24*/ new RowCol(5,2),
        /*25*/ new RowCol(5,3), /*26*/ new RowCol(5,4), /*27*/ new RowCol(5,5), /*28*/ new RowCol(5,6), /*29*/ new RowCol(6,0), /*30*/ new RowCol(6,1),
        /*31*/ new RowCol(6,2),
        /*JAN*/ new RowCol(0,0), /*FEB*/ new RowCol(0,1), /*MAR*/ new RowCol(0,2), /*APR*/ new RowCol(0,3),
        /*MAY*/ new RowCol(0,4), /*JUN*/ new RowCol(0,5), /*JUL*/ new RowCol(1,0), /*AUG*/ new RowCol(1,1),
        /*SEP*/ new RowCol(1,2), /*OCT*/ new RowCol(1,3), /*NOV*/ new RowCol(1,4), /*DEC*/ new RowCol(1,5)
    };
}