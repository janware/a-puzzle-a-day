﻿using PuzzleSolver;

const string file = @"calendar.solutions.txt";

System.Console.WriteLine("Solving Calendar Puzzle");

if (File.Exists(file))
{
    File.Delete(file);
    File.CreateText(file);
}

SolvePuzzle();


void SolvePuzzle()

{
    var situation = new Situation();
    const int maxPos = 42;

    // A
    situation.Shapes.Add(new Shape('A'));
    for (var posA = 0; posA < maxPos; posA++)
    {
        situation.Shapes[0].Position = posA;
        for (var orA = 0; orA < situation.Shapes[0].Orientations.Length; orA++)
        {
            situation.Shapes[0].Orientation = orA;
            if (!situation.IsValid()) continue;

            // B
            situation.Shapes.Add(new Shape('B'));
            for (var posB = 0; posB < maxPos; posB++)
            {
                situation.Shapes[1].Position = posB;
                for (var orB = 0; orB < situation.Shapes[1].Orientations.Length; orB++)
                {
                    situation.Shapes[1].Orientation = orB;
                    if (!situation.IsValid()) continue;

                    // C
                    situation.Shapes.Add(new Shape('C'));
                    for (var posC = 0; posC < maxPos; posC++)
                    {
                        situation.Shapes[2].Position = posC;
                        for (var orC = 0; orC < situation.Shapes[2].Orientations.Length; orC++)
                        {
                            situation.Shapes[2].Orientation = orC;
                            if (!situation.IsValid()) continue;

                            // D
                            situation.Shapes.Add(new Shape('D'));
                            for (var posD = 0; posD < maxPos; posD++)
                            {
                                situation.Shapes[3].Position = posD;
                                for (var orD = 0; orD < situation.Shapes[3].Orientations.Length; orD++)
                                {
                                    situation.Shapes[3].Orientation = orD;
                                    if (!situation.IsValid()) continue;

                                    // E
                                    situation.Shapes.Add(new Shape('E'));
                                    for (var posE = 0; posE < maxPos; posE++)
                                    {
                                        situation.Shapes[4].Position = posE;
                                        for (var orE = 0; orE < situation.Shapes[4].Orientations.Length; orE++)
                                        {
                                            situation.Shapes[4].Orientation = orE;
                                            if (!situation.IsValid()) continue;

                                            // F
                                            situation.Shapes.Add(new Shape('F'));
                                            for (var posF = 0; posF < maxPos; posF++)
                                            {
                                                situation.Shapes[5].Position = posF;
                                                for (var orF = 0; orF < situation.Shapes[5].Orientations.Length; orF++)
                                                {
                                                    situation.Shapes[5].Orientation = orF;
                                                    if (!situation.IsValid()) continue;

                                                    // G
                                                    situation.Shapes.Add(new Shape('G'));
                                                    for (var posG = 0; posG < maxPos; posG++)
                                                    {
                                                        situation.Shapes[6].Position = posG;
                                                        for (var orG = 0;
                                                             orG < situation.Shapes[6].Orientations.Length;
                                                             orG++)
                                                        {
                                                            situation.Shapes[6].Orientation = orG;
                                                            if (!situation.IsValid()) continue;

                                                            // H
                                                            situation.Shapes.Add(new Shape('H'));
                                                            for (var posH = 0; posH < maxPos; posH++)
                                                            {
                                                                situation.Shapes[7].Position = posH;
                                                                for (var orH = 0;
                                                                     orH < situation.Shapes[7].Orientations.Length;
                                                                     orH++)
                                                                {
                                                                    situation.Shapes[7].Orientation = orH;
                                                                    if (!situation.IsValid()) continue;

                                                                    CheckSituation(situation);
                                                                }
                                                            }

                                                            situation.Shapes.RemoveAll(sh => sh.Name == 'H');
                                                        }
                                                    }

                                                    situation.Shapes.RemoveAll(sh => sh.Name == 'G');
                                                }
                                            }

                                            situation.Shapes.RemoveAll(sh => sh.Name == 'F');
                                        }
                                    }

                                    situation.Shapes.RemoveAll(sh => sh.Name == 'E');
                                }
                            }

                            situation.Shapes.RemoveAll(sh => sh.Name == 'D');
                        }
                    }

                    situation.Shapes.RemoveAll(sh => sh.Name == 'C');
                }
            }

            situation.Shapes.RemoveAll(sh => sh.Name == 'B');
        }
    }
}

void CheckSituation(Situation situation)
{
    if (!situation.IsValid()) return;
    using (var sw = File.AppendText(file))
    {
        sw.WriteLine(situation.Write());
    }

    System.Console.WriteLine(situation.Write());
}