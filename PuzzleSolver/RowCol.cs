namespace PuzzleSolver;

internal class RowCol
{
    public int Row { get; }
    public int Col { get; }

    public RowCol(int row, int col)
    {
        this.Row = row;
        this.Col = col;
    }
}