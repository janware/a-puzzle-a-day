namespace PuzzleSolver;

public class Shape
{
    internal readonly char Name;
    internal int Orientation;
    internal readonly RowCol[][] Orientations;
    internal int Position;

    public Shape(char name)
    {
        Name = name;
        Position = 0;
        Orientation = 0;
        Orientations = ReadOrientationsFor(Name);
    }

    public Shape(char name, int position, int orientation) : this(name)
    {
        Position = position;
        Orientation = orientation;
    }

    private static RowCol[][] ReadOrientationsFor(char c)
    {
        var shapeLines = ShapesFile.Definitions[c];
        var shapeBlockSize = ReadBlockSizeFrom(shapeLines);
        var shapeSize = ReadSizeFrom(shapeLines);
        var numberOfOrientation = ReadNumberFrom(shapeLines);
        var result = new RowCol[numberOfOrientation][];
        for (var i = 0; i < numberOfOrientation; i++)
        {
            result[i] = ReadOrientationFrom(i, shapeLines, shapeBlockSize, shapeSize);
        }

        return result;
    }

    private static RowCol[] ReadOrientationFrom(int i, IEnumerable<string> lines, int blockSize, int shapeSize)
    {
        var block = ReadBlockFrom(i, lines, blockSize);
        var result = ReadOrientationFromBlock(block, shapeSize);
        return result;
    }

    private static RowCol[] ReadOrientationFromBlock(char[,] block, int shapeSize)
    {
        var result = new RowCol[shapeSize];
        var shapeIndex = 0;
        var firstXy = new RowCol(0, 0);
        var blockSize = 1 + block.GetUpperBound(0);
        for (var i = 0; i < blockSize; i++)
        {
            for (var j = 0; j < blockSize; j++)
            {
                if (block[i, j] == ' ') continue;
                if (shapeIndex == 0)
                {
                    result[0] = new RowCol(0, 0);
                    shapeIndex = 1;
                    firstXy = new RowCol(i, j);
                    continue;
                }

                result[shapeIndex] = new RowCol(i - firstXy.Row, j - firstXy.Col);
                shapeIndex++;
            }
        }

        return result;
    }

    private static char[,] ReadBlockFrom(int i, IEnumerable<string> lines, int size)
    {
        var result = new char[size, size];
        var startPos = i * (size + 2);
        var m = 0;
        foreach (var line in lines.Skip(1))
        {
            for (var j = 0; j < size; j++)
            {
                result[m, j] = line[j + startPos];
            }

            m++;
        }

        return result;
    }

    private static int ReadSizeFrom(IEnumerable<string> lines)
    {
        var result = lines.Skip(1).Sum(line => line.Count(c => c != ' '));

        return result / ReadNumberFrom(lines);
    }

    private static int ReadNumberFrom(IEnumerable<string> lines)
    {
        var line = lines.First();
        var countPlus = line.Count(c => c == '+');
        return 1 + countPlus / 2;
    }

    private static int ReadBlockSizeFrom(IEnumerable<string> lines)
    {
        var line = lines.First();
        return line.IndexOf('+');
    }

    public bool IsOnBoard()
    {
        var cell = Board.Positions[Position];
        foreach (var point in Orientations[Orientation])
        {
            var testX = cell.Row + point.Row;
            var testY = cell.Col + point.Col;
            if (testX < 0 || testY < 0 || testX > 6 || testY > 6) return false;
            if (Board.Values[testX, testY] == 0) return false;
        }

        return true;
    }

    public bool Overlaps(Shape otherShape)
    {
        var firstCell = Board.Positions[Position];
        var cellsOfThisShape = Orientations[Orientation]
            .Select(point => new RowCol(firstCell.Row + point.Row, firstCell.Col + point.Col)).ToList();
        var firstCellOfOtherShape = Board.Positions[otherShape.Position];
        var cellsOfOtherShape = otherShape.Orientations[otherShape.Orientation]
            .Select(point => new RowCol(firstCellOfOtherShape.Row + point.Row, firstCellOfOtherShape.Col + point.Col)).ToList();
        return cellsOfOtherShape.Any(cell => cellsOfThisShape.Exists(c => c.Row == cell.Row && c.Col == cell.Col));
    }
}