namespace PuzzleSolver;

public static class ShapesFile
{
    public static readonly Dictionary<char, List<string>> Definitions = new();

    static ShapesFile()
    {
        var currentChar = default(char);
        foreach (var line in File.ReadAllLines(@"shapes.txt"))
        {
            if (line.StartsWith("//")) continue;
            if (line.StartsWith('-'))
            {
                currentChar = line[1];
                Definitions.Add(currentChar,new List<string>());
            }

            Definitions[currentChar].Add(line);
        }
    }
}