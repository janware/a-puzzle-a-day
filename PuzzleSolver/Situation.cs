namespace PuzzleSolver;

public class Situation
{
    public readonly List<Shape> Shapes = new();

    public bool IsValid()
    {
        return (AllShapesOnBoard() && NoneOverlapping());
    }

    public bool NoneOverlapping()
    {
        var numberOfShapes = Shapes.Count;
        for (var i = 0; i < numberOfShapes; i++)
        {
            for (var j = i + 1; j < numberOfShapes; j++)
            {
                if (Shapes[i].Overlaps(Shapes[j])) return false;
            }
        }

        return true;
    }

    public bool AllShapesOnBoard()
    {
        return Shapes.All(shape => shape.IsOnBoard());
    }

    public (int, int) GetHoles()
    {
        var positions = Board.Positions.ToList();
        foreach (var shape in Shapes)
        {
            var firstCell = Board.Positions[shape.Position];
            foreach (var point in shape.Orientations[shape.Orientation])
            {
                var cellX = firstCell.Row + point.Row;
                var cellY = firstCell.Col + point.Col;
                positions.RemoveAll(p => p.Row == cellX && p.Col == cellY);
            }
        }

        var first = Board.Positions.ToList().FindIndex(0, p => p.Row == positions[0].Row && p.Col == positions[0].Col);
        var second = Board.Positions.ToList().FindIndex(0, p => p.Row == positions[1].Row && p.Col == positions[1].Col);
        return (first, second);
    }
    
    public string Write()
    {
        var months = new string[12]
            { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        var holes = GetHoles();
        var hole1 = holes.Item1 < 31 ? (holes.Item1 + 1).ToString() : months[holes.Item1 - 31];
        var hole2 = holes.Item2 < 31 ? (holes.Item2 + 1).ToString() : months[holes.Item2 - 31];
        var result = $"Holes: ({hole1},{hole2}), A pos {Shapes[0].Position} orientation {Shapes[0].Orientation}";
        result = $"{result}, B pos {Shapes[1].Position} orientation {Shapes[1].Orientation}";
        result = $"{result}, C pos {Shapes[2].Position} orientation {Shapes[2].Orientation}";
        result = $"{result}, D pos {Shapes[3].Position} orientation {Shapes[3].Orientation}";
        result = $"{result}, E pos {Shapes[4].Position} orientation {Shapes[4].Orientation}";
        result = $"{result}, F pos {Shapes[5].Position} orientation {Shapes[5].Orientation}";
        result = $"{result}, G pos {Shapes[6].Position} orientation {Shapes[6].Orientation}";
        result = $"{result}, H pos {Shapes[7].Position} orientation {Shapes[7].Orientation}";
        return result;
    }
}