using System.Runtime.InteropServices;
using PuzzleSolver;
using FluentAssertions;

namespace TestProject1;

public class ShapeTests
{
    [Theory]
    [InlineData('A', 0, 0, true)]
    [InlineData('A', 0, 34, false)]
    [InlineData('B', 1, 16, true)]
    [InlineData('B', 1, 22, false)]
    [InlineData('F', 3, 39, true)]
    [InlineData('F', 3, 1, false)]
    [InlineData('F', 3, 19, false)]
    [InlineData('F', 3, 4, true)]
    [InlineData('G', 4, 5, true)]
    [InlineData('G', 4, 9, false)]
    [InlineData('G', 4, 30, false)]
    [InlineData('G', 4, 36, true)]
    public void Test_Shape_On_Board_Theory(char shape, int orientation, int position, bool expectedResult)
    {
        // arrange
        var shapeUnderTest = new Shape(shape, position, orientation);

        // act
        var isOnBoard = shapeUnderTest.IsOnBoard();

        // assert
        isOnBoard.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData('A', 0, 0, 'B', 0, 0, true)]
    [InlineData('B', 1, 1, 'C', 0, 32, false)]
    [InlineData('C', 1, 1, 'D', 3, 5, true)]
    [InlineData('D', 2, 31, 'E', 0, 21, false)]
    [InlineData('E', 1, 11, 'F', 3, 6, true)]
    [InlineData('F', 3, 3, 'G', 6, 34, false)]
    public void Test_Overlapping_Shapes_Theory(char shape1, int orientation1, int position1, char shape2,
        int orientation2, int position2, bool expectedResult)
    {
        // arrange
        var shapeUnderTest1 = new Shape(shape1, position1, orientation1);
        var shapeUnderTest2 = new Shape(shape2, position2, orientation2);

        // act
        var isOverlapping = shapeUnderTest1.Overlaps(shapeUnderTest2);

        // assert
        isOverlapping.Should().Be(expectedResult);
    }
}