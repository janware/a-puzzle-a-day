using PuzzleSolver;
using FluentAssertions;

namespace TestProject1;

public class SituationTests
{
    [Fact]
    public void Init_Sets_Proper_InitialSituation()
    {
        // arrange
        var situation = new Situation();

        // assert
        situation.Shapes.Should().HaveCount(0);
    }

    [Fact]
    public void Test_All_Shapes_On_Board_False()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 40, 5));
        situation.Shapes.Add(new Shape('B', 10, 0));
        situation.Shapes.Add(new Shape('C', 18, 1));
        situation.Shapes.Add(new Shape('D', 31, 2));
        situation.Shapes.Add(new Shape('E', 21, 0));
        situation.Shapes.Add(new Shape('F', 0, 0));
        situation.Shapes.Add(new Shape('G', 35, 1));
        situation.Shapes.Add(new Shape('H', 15, 6));

        // act
        var result = situation.AllShapesOnBoard();

        // assert
        result.Should().Be(false);
    }

    [Fact]
    public void Test_All_Shapes_On_Board_False_2()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 40, 5));
        situation.Shapes.Add(new Shape('H', 21, 4));

        // act
        var result = situation.AllShapesOnBoard();

        // assert
        result.Should().Be(false);
    }

    [Fact]
    public void Test_All_Shapes_On_Board_True()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 40, 5));
        situation.Shapes.Add(new Shape('B', 10, 0));
        situation.Shapes.Add(new Shape('C', 18, 1));
        situation.Shapes.Add(new Shape('D', 31, 2));
        situation.Shapes.Add(new Shape('E', 21, 0));
        situation.Shapes.Add(new Shape('F', 0, 0));
        situation.Shapes.Add(new Shape('G', 34, 1));
        situation.Shapes.Add(new Shape('H', 15, 6));

        // act
        var result = situation.AllShapesOnBoard();

        // assert
        result.Should().Be(true);
    }

    [Fact]
    public void Test_NoneOverlapping_False()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 40, 5));
        situation.Shapes.Add(new Shape('B', 10, 0));
        situation.Shapes.Add(new Shape('C', 18, 1));
        situation.Shapes.Add(new Shape('D', 31, 2));
        situation.Shapes.Add(new Shape('E', 21, 0));
        situation.Shapes.Add(new Shape('F', 0, 0));
        situation.Shapes.Add(new Shape('G', 34, 1));
        situation.Shapes.Add(new Shape('H', 15, 6));

        // act
        var result = situation.NoneOverlapping();

        // assert
        result.Should().Be(false);
    }

    [Fact]
    public void Test_NoneOverlapping_True()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 6, 7));
        situation.Shapes.Add(new Shape('B', 16, 2));
        situation.Shapes.Add(new Shape('C', 34, 3));
        situation.Shapes.Add(new Shape('D', 37, 2));
        situation.Shapes.Add(new Shape('E', 7, 1));
        situation.Shapes.Add(new Shape('F', 42, 3));
        situation.Shapes.Add(new Shape('G', 23, 3));
        situation.Shapes.Add(new Shape('H', 32, 2));

        // act
        var result = situation.NoneOverlapping();

        // assert
        result.Should().Be(true);
    }

    [Fact]
    public void Test_Get_Holes1()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 6, 7));
        situation.Shapes.Add(new Shape('B', 16, 2));
        situation.Shapes.Add(new Shape('C', 34, 3));
        situation.Shapes.Add(new Shape('D', 37, 2));
        situation.Shapes.Add(new Shape('E', 7, 1));
        situation.Shapes.Add(new Shape('F', 42, 3));
        situation.Shapes.Add(new Shape('G', 23, 3));
        situation.Shapes.Add(new Shape('H', 32, 2));

        // act
        var result = situation.GetHoles();

        // assert
        result.Item1.Should().Be(31);
        result.Item2.Should().Be(36);

        var text = situation.Write();
        text.Should().Contain("(Jan,Jun)");
    }

    [Fact]
    public void Test_Get_Holes2()
    {
        // arrange
        var situation = new Situation();
        situation.Shapes.Add(new Shape('A', 6, 7));
        situation.Shapes.Add(new Shape('B', 16, 2));
        situation.Shapes.Add(new Shape('C', 3, 1));
        situation.Shapes.Add(new Shape('D', 34, 3));
        situation.Shapes.Add(new Shape('E', 7, 1));
        situation.Shapes.Add(new Shape('F', 31, 0));
        situation.Shapes.Add(new Shape('G', 23, 3));
        situation.Shapes.Add(new Shape('H', 35, 4));

        // act
        var result = situation.GetHoles();

        // assert
        situation.IsValid().Should().BeTrue();

        result.Item1.Should().Be(2);
        result.Item2.Should().Be(9);

        var text = situation.Write();
        text.Should().Contain("(3,10)");
    }
}